const Joi = require('joi');

const userValidator = Joi.object().keys({
  oldPassword: Joi.string()
      .pattern(/^[a-zA-Z0-9]{3,}$/)
      .required(),

  newPassword: Joi.string()
      .pattern(/^[a-zA-Z0-9]{3,}$/)
      .required(),
});

module.exports = userValidator;
