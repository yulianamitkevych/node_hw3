const Joi = require('joi');

const logInValidator = Joi.object().keys({
  email: Joi.string()
      .email({minDomainSegments: 2})
      .required(),

  password: Joi.string()
      .pattern(/^[a-zA-Z0-9]{3,}$/)
      .required(),
});

const registerValidator = logInValidator.keys({
  role: Joi.string()
      .valid('SHIPPER', 'DRIVER')
      .required(),
});

module.exports = {
  registerValidator,
  logInValidator,
};
