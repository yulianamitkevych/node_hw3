const Joi = require('joi');

const truckTypeValidator = Joi.object().keys({
  type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .required(),
});

module.exports = {
  truckTypeValidator,
};
