const Joi = require('joi');

const loadValidator = Joi.object().keys({
  name: Joi.string()
      .required(),
  payload: Joi.number()
      .required(),
  pickup_address: Joi.string()
      .required(),
  delivery_address: Joi.string()
      .required(),
  dimensions: {
    width: Joi.number()
        .required(),
    length: Joi.number()
        .required(),
    height: Joi.number()
        .required(),
  },
});

module.exports = {
  loadValidator,
};
