const {isDriverAbleToChange} = require('../helper/isDriverAbleToChange');

const checkDriverChangeAbility = async (req, res, next) => {
  try {
    const {_id} = req.currentUser;

    if (!await isDriverAbleToChange(_id)) {
      return res
          .status(400)
          .json({
            message: 'Driver isn\'t able to change any data now',
          });
    }

    next();
  } catch (error) {
    res
        .status(500)
        .json({
          message: 'Internal server error',
        });
  }
};

module.exports = {checkDriverChangeAbility};
