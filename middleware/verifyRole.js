const verifyRole = (neededRole) => {
  return function(req, res, next) {
    try {
      const {role} = req.currentUser;

      if (role !== neededRole) {
        return res
            .status(400)
            .json({
              message: `Only user with role ${neededRole} has permission`,
            });
      }

      next();
    } catch (error) {
      res
          .status(500)
          .json({
            message: 'Internal server error',
          });
    }
  };
};

const verifyDriver = verifyRole('DRIVER');
const verifyShipper = verifyRole('SHIPPER');

module.exports = {verifyDriver, verifyShipper};
