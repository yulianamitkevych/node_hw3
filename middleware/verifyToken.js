const jwt = require('jsonwebtoken');
const user = require('../models/user');

require('dotenv').config();
const {TOKEN_KEY} = process.env;

const verifyToken = async (req, res, next) => {
  try {
    const authorization = req.headers.authorization;

    if (!authorization) {
      return res.status(400).json({
        message: 'Token wasn\'t sent!',
      });
    }

    try {
      const token = authorization.split(' ')[1];
      const decodedPayload = jwt.verify(token, TOKEN_KEY);

      const givenUser = await user.findById(decodedPayload._id);
      if (!givenUser) {
        return res.status(400).json({
          message: 'User doesn\'t exist!',
        });
      }

      req.currentUser = givenUser;
    } catch (error) {
      return res.status(400).json({
        message: 'Invalid token!',
      });
    }

    return next();
  } catch (error) {
    return res.status(500).json({
      message: 'Internal server error on token verification step!',
    });
  }
};

module.exports = verifyToken;
