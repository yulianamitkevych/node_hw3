const mongoose = require('mongoose');

const truck = new mongoose.Schema({
  created_by: {type: String},
  assigned_to: {type: String, default: ''},
  type: {type: String},
  status: {type: String},
  created_date: {type: String},
});

module.exports = mongoose.model('trucks', truck);
