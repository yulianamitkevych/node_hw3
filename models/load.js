const mongoose = require('mongoose');

const log = new mongoose.Schema({
  message: {type: String},
  time: {type: String},
}, {_id: false});

const load = new mongoose.Schema({
  created_by: {type: String},
  assigned_to: {type: String, default: ''},
  status: {type: String, default: 'NEW'},
  state: {type: String, default: ''},
  name: {type: String},
  payload: {type: Number},
  pickup_address: {type: String},
  delivery_address: {type: String},
  dimensions: {
    width: {type: Number},
    length: {type: Number},
    height: {type: Number},
  },
  logs: [
    log,
  ],
  created_date: {type: String},
});

module.exports = mongoose.model('loads', load);
