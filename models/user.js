const mongoose = require('mongoose');

const user = new mongoose.Schema({
  role: {type: String},
  email: {type: String},
  password: {type: String},
  created_date: {type: String},
});

module.exports = mongoose.model('users', user);
