const isLoadNotNew = (load) => {
    if (load.status !== 'NEW') {
        throw new Error;
    }
};

module.exports = {
    isLoadNotNew,
};
