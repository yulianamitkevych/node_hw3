const truck = require('../models/truck');

const truckType = {
  'SPRINTER': {
    dimensions: {
      width: 300,
      length: 250,
      height: 170,
    },
    payload: 1700,
  },
  'SMALL STRAIGHT': {
    dimensions: {
      width: 500,
      length: 250,
      height: 170,
    },
    payload: 2500,
  },
  'LARGE STRAIGHT': {
    dimensions: {
      width: 700,
      length: 350,
      height: 200,
    },
    payload: 4000,
  },
};

const findDriver = async (currentLoad) => {
  const arrayOfAssignedTrucks = await truck.find({
    assigned_to: {$ne: ''},
    status: 'IS',
  });
  return arrayOfAssignedTrucks.find((el) => {
    return currentLoad.payload < truckType[el.type].payload &&
    currentLoad.dimensions.width < truckType[el.type].dimensions.width &&
    currentLoad.dimensions.length < truckType[el.type].dimensions.length &&
    currentLoad.dimensions.height < truckType[el.type].dimensions.height;
  });
};

module.exports = {
  findDriver,
};
