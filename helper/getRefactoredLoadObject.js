const getRefactoredLoadObject = (load) => {
  return {
    _id: load._id,
    created_by: load.created_by,
    assigned_to: load.assigned_to,
    status: load.status,
    state: load.state,
    name: load.name,
    payload: load.payload,
    pickup_address: load.pickup_address,
    delivery_address: load.delivery_address,
    dimensions: load.dimensions,
    logs: load.logs,
    created_date: load.created_date,
  };
};

module.exports = {
  getRefactoredLoadObject,
};
