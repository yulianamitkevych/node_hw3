const isObjectEmpty = (obj) => {
    if (Object.keys(obj).length === 0) {
        throw new Error;
    }
};

module.exports = {
    isObjectEmpty,
};
