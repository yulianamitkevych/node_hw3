const truck = require('../models/truck');

const resetAssigned = async (userId, truckId) => {
  await truck
      .findOneAndUpdate({assigned_to: userId}, {assigned_to: ''})
      .where('_id')
      .ne(truckId);
};

module.exports = {
  resetAssigned,
};
