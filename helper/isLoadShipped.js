const isLoadShipped = (load) => {
  if (load.status === 'SHIPPED') {
    throw new Error;
  }
};

module.exports = {
  isLoadShipped,
};
