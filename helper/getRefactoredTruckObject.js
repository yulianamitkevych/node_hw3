const getRefactoredTruckObject = (truck) => {
  return {
    _id: truck._id,
    created_by: truck.created_by,
    assigned_to: truck.assigned_to,
    type: truck.type,
    status: truck.status,
    created_date: truck.created_date,
  };
};

module.exports = {
  getRefactoredTruckObject,
};
