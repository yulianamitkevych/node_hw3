const truck = require('../models/truck');

const isDriverAbleToChange = async (driverId) => {
  return !await truck.findOne({assigned_to: driverId, status: 'OL'});
};

module.exports = {
  isDriverAbleToChange,
};
