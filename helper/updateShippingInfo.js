const load = require('../models/load');
const moment = require('moment');

const updateShippingInfo = async (
    loadId,
    status,
    message,
    assigned_to = undefined,
    state = undefined,
) => {
  const objectOfUpdatedValues = {
    status,
    $push: {
      logs: {
        message,
        time: moment().utc().format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'),
      },
    },
  };

  if (assigned_to) {
    objectOfUpdatedValues.assigned_to = assigned_to;
  }

  if (state) {
    objectOfUpdatedValues.state = state;
  }

  await load.findByIdAndUpdate(loadId, objectOfUpdatedValues);
};

module.exports = {
  updateShippingInfo,
};
