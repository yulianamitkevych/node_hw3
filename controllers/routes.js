const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const verifyToken = require('../middleware/verifyToken');
const authController = require('./auth/authController');
const userController = require('./user/userController');
const truckController = require('./truck/truckController');
const loadController = require('./load/loadController');

require('dotenv').config();
require('../config/DB').connect();

const routes = express();

routes.use(express.json());
routes.use(morgan('combined'));
routes.use(cors());
routes.use(cookieParser());

routes.use('/api/auth', authController);
routes.use('/api/users/me', verifyToken, userController);
routes.use('/api/trucks', verifyToken, truckController);
routes.use('/api/loads', verifyToken, loadController);

routes.use((req, res) => {
    res.status(400).json({
        message: 'Bad request',
    });
});

module.exports = routes;
