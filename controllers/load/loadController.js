const express = require('express');
const moment = require('moment');
const router = express.Router();
const load = require('../../models/load');
const truck = require('../../models/truck');
const {loadValidator} = require('../../validations/loadValidator');
const {verifyShipper, verifyDriver} = require('../../middleware/verifyRole');
const {getRefactoredLoadObject} = require('../../helper/getRefactoredLoadObject');
const {getRefactoredTruckObject} = require('../../helper/getRefactoredTruckObject');
const {findDriver} = require('../../helper/findDriver');
const {updateShippingInfo} = require('../../helper/updateShippingInfo');
const {isObjectEmpty} = require('../../helper/isObjectEmpty');
const {isLoadShipped} = require('../../helper/isLoadShipped');
const {isLoadNotNew} = require('../../helper/isLoadNotNew');

const loadStates = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
];

router.get('/', async (req, res) => {
    try {
        let {status, limit = 10, offset = 0} = req.query;

        if (limit > 50) {
            limit = 50;
        }

        let arrayOfLoads;
        const {role, _id} = req.currentUser;

        if (role === 'SHIPPER') {
            arrayOfLoads = await load.find({created_by: _id});
        } else {
            arrayOfLoads = await load.find({assigned_to: _id});
        }

        let filteredArray = arrayOfLoads.slice(offset, limit + offset);

        if (status) {
            filteredArray = filteredArray.filter((el) => el.status === status);
        }

        const refactoredArrayOfLoads = filteredArray.map((el) => {
            return getRefactoredLoadObject(el);
        });

        res.json({
            'loads': [...refactoredArrayOfLoads],
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.post('/', verifyShipper, async (req, res) => {
    try {
        const {_id} = req.currentUser;

        const postedData = req.body;
        const {error} = loadValidator.validate(postedData);

        if (error) {
            return res
                .status(400)
                .json({
                    message: 'Wrong posted data',
                });
        }

        await load.create({
            created_by: _id,
            name: postedData.name,
            payload: postedData.payload,
            pickup_address: postedData.pickup_address,
            delivery_address: postedData.delivery_address,
            dimensions: postedData.dimensions,
            logs: [],
            created_date: moment().utc().format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'),
        });

        res.json({
            message: 'Load created successfully',
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.get('/active',
    verifyDriver,
    async (req, res) => {
        try {
            const {_id} = req.currentUser;
            const currentLoad = await load.findOne({
                assigned_to: _id, status: 'ASSIGNED',
            });

            if (!currentLoad) {
                return res.json({
                    load: {},
                });
            }

            res.json({
                load: getRefactoredLoadObject(currentLoad),
            });
        } catch (error) {
            res
                .status(500)
                .json({
                    message: 'Internal server error',
                });
        }
    });

router.patch('/active/state', verifyDriver, async (req, res) => {
    try {
        const {_id: userId} = req.currentUser;

        const currentLoad = await load.findOne({
            assigned_to: userId, status: 'ASSIGNED',
        });
        if (!currentLoad) {
            return res.json({
                message: 'Driver doesn\'t have active load',
            });
        }

        const currentIndexOfState = loadStates.indexOf(currentLoad.state);
        const updatedState = loadStates[currentIndexOfState + 1];
        await load.findByIdAndUpdate(currentLoad._id, {
            state: updatedState,
        });

        if (updatedState === loadStates[loadStates.length - 1]) {
            await truck.findOneAndUpdate({assigned_to: userId}, {
                status: 'IS',
            });
            await updateShippingInfo(currentLoad._id, 'SHIPPED', 'Load was shipped');
        }

        res.json({
            message: `Load state changed to '${updatedState}'`,
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.get('/:id', async (req, res) => {
    try {
        const loadId = req.params.id;
        let currentLoad;

        try {
            currentLoad = await load.findById(loadId);
            isObjectEmpty(currentLoad);
        } catch (error) {
            return res
                .status(400)
                .json({
                    message: 'Bad request',
                });
        }

        res.json({
            'load': getRefactoredLoadObject(currentLoad),
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.put('/:id', verifyShipper, async (req, res) => {
    try {
        const loadId = req.params.id;

        const postedData = req.body;
        const {error} = loadValidator.validate(postedData);

        if (error) {
            return res
                .status(400)
                .json({
                    message: 'Wrong posted data',
                });
        }

        try {
            const currentLoad = await load.findById(loadId);
            isLoadNotNew(currentLoad);
            await load.findByIdAndUpdate(loadId, postedData);
        } catch (error) {
            return res
                .status(400)
                .json({
                    message: 'Bad request',
                });
        }

        res.json({
            message: 'Load details changed successfully',
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.delete('/:id', verifyShipper, async (req, res) => {
    try {
        const loadId = req.params.id;

        try {
            const currentLoad = await load.findById(loadId);
            isLoadNotNew(currentLoad);
            await load.findByIdAndDelete(loadId);
        } catch (error) {
            return res
                .status(400)
                .json({
                    message: 'Bad request',
                });
        }

        res.json({
            message: 'Load deleted successfully',
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.post('/:id/post', verifyShipper, async (req, res) => {
    try {
        const loadId = req.params.id;
        let currentLoad;

        try {
            currentLoad = await load.findById(loadId);
            isObjectEmpty(currentLoad);
            isLoadShipped(currentLoad);
        } catch (error) {
            return res
                .status(400)
                .json({
                    message: 'Bad request',
                });
        }

        await updateShippingInfo(currentLoad._id, 'POSTED', 'Load was posted');

        const foundTruck = await findDriver(currentLoad);
        if (!foundTruck) {
            await updateShippingInfo(
                currentLoad._id,
                'NEW',
                'Driver wasn\'t found so backed to status NEW',
            );
        } else {
            await truck.findByIdAndUpdate(foundTruck._id, {
                status: 'OL',
            });
            await updateShippingInfo(
                currentLoad._id,
                'ASSIGNED',
                `Load assigned to driver with id ${foundTruck.assigned_to}`,
                foundTruck.assigned_to,
                loadStates[0],
            );
        }

        res.json({
            message: 'Load posted successfully',
            driver_found: !!foundTruck,
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.get('/:id/shipping_info', verifyShipper, async (req, res) => {
    try {
        const loadId = req.params.id;
        let currentLoad;
        let currentTruck = {};

        try {
            currentLoad = await load.findById(loadId);
            isObjectEmpty(currentLoad);
            isLoadShipped(currentLoad);
        } catch (error) {
            return res
                .status(400)
                .json({
                    message: 'Bad request',
                });
        }

        if (currentLoad.status === 'ASSIGNED') {
            currentTruck = await truck.findOne({
                assigned_to: currentLoad.assigned_to,
                status: 'OL',
            });
        }

        res.json({
            load: getRefactoredLoadObject(currentLoad),
            truck: getRefactoredTruckObject(currentTruck),
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

module.exports = router;
