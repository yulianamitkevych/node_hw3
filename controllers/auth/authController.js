const express = require('express');
const moment = require('moment');
const bcrypt = require('bcrypt');
const user = require('../../models/user');
const jwt = require('jsonwebtoken');
const {registerValidator, logInValidator} = require('../../validations/authValidator');
const router = express.Router();

const bcryptSalt = 14;

router.post('/register', async (req, res) => {
    try {
        const {email, password, role} = req.body;

        const {error} = registerValidator.validate({email, password, role});

        if (error) {
            return res
                .status(400)
                .json({
                    message: 'Wrong credentials',
                });
        }

        const existedUser = await user.findOne({email});

        if (existedUser) {
            return res
                .status(400)
                .json({
                    message: 'User with passed email is already existed',
                });
        }

        await user.create({
            role,
            email,
            password: await bcrypt.hash(password, bcryptSalt),
            created_date: moment().utc().format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'),
        });

        res
            .status(200)
            .json({
                message: 'Profile created successfully',
            });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.post('/login', async (req, res) => {
    try {
        const {email, password} = req.body;

        const {error} = logInValidator.validate({email, password});
        if (error) {
            return res
                .status(400)
                .json({
                    message: 'Wrong credentials',
                });
        }

        const existedUser = await user.findOne({email});

        if (existedUser && (await bcrypt.compare(password, existedUser.password))) {
            return res
                .status(200)
                .json({
                    jwt_token: jwt.sign(
                        {
                            _id: existedUser._id,
                        },
                        process.env.TOKEN_KEY,
                    ),
                });
        }

        res
            .status(400)
            .json({
                message: 'Wrong credentials',
            });
    } catch (err) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

module.exports = router;
