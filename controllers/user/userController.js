const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();
const user = require('../../models/user');
const userValidator = require('../../validations/userValidator');
const {isDriverAbleToChange} = require('../../helper/isDriverAbleToChange');

const bcryptSalt = 14;

router.get('/', async (req, res) => {
  try {
    const {_id, role, email, created_date} = req.currentUser;

    res.json({
      user: {
        _id,
        role,
        email,
        created_date,
      },
    });
  } catch (error) {
    res
        .status(500)
        .json({
          message: 'Internal server error',
        });
  }
});

router.delete('/', async (req, res) => {
  try {
    const {_id} = req.currentUser;

    if (!await isDriverAbleToChange(_id)) {
      return res
          .status(400)
          .json('Driver isn\'t able to change any data now');
    }

    await user.deleteOne(req.currentUser);

    res.json({
      message: 'Profile deleted successfully',
    });
  } catch (error) {
    res
        .status(500)
        .json({
          message: 'Internal server error',
        });
  }
});

router.patch('/password', async (req, res) => {
  try {
    const {_id, password} = req.currentUser;

    if (!await isDriverAbleToChange(_id)) {
      return res
          .status(400)
          .json('Driver isn\'t able to change any data now');
    }

    const {oldPassword, newPassword} = req.body;

    const {error} = userValidator.validate({oldPassword, newPassword});

    if (error) {
      return res
          .status(400)
          .json({
            message: 'Entered password values are not valid',
          });
    }

    if (!await bcrypt.compare(oldPassword, password)) {
      return res
          .status(400)
          .json({
            message: 'Wrong old password',
          });
    }

    await user.findByIdAndUpdate(
        _id,
        {
          password: await bcrypt.hash(newPassword, bcryptSalt),
        },
    );

    res.json({
      message: 'Password changed successfully',
    });
  } catch (error) {
    res
        .status(500)
        .json({
          message: 'Internal server error',
        });
  }
});

module.exports = router;
