const express = require('express');
const moment = require('moment');
const router = express.Router();
const truck = require('../../models/truck');
const {truckTypeValidator} = require('../../validations/truckValidator');
const {verifyDriver} = require('../../middleware/verifyRole');
const {getRefactoredTruckObject} = require('../../helper/getRefactoredTruckObject');
const {resetAssigned} = require('../../helper/resetAssigned');
const {checkDriverChangeAbility} = require('../../middleware/checkDriverChangeAbility');
const {isObjectEmpty} = require('../../helper/isObjectEmpty');

router.use(verifyDriver);

router.get('/', async (req, res) => {
    try {
        const arrayOfTrucks = await truck.find();

        const refactoredArrayOfTrucks = arrayOfTrucks.map((el) => {
            return getRefactoredTruckObject(el);
        });

        res.json({
            'trucks': [...refactoredArrayOfTrucks],
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.post('/', checkDriverChangeAbility, async (req, res) => {
    try {
        const {_id} = req.currentUser;
        const {type} = req.body;
        const {error} = truckTypeValidator.validate({type});

        if (error) {
            return res
                .status(400)
                .json({
                    message: 'Wrong truck type',
                });
        }

        await truck.create({
            created_by: _id,
            type,
            status: 'IS',
            created_date: moment().utc().format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'),
        });

        res.json({
            message: 'Truck created successfully',
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.get('/:id', async (req, res) => {
    try {
        const truckId = req.params.id;
        let currentTruck;

        try {
            currentTruck = await truck.findById(truckId);
            isObjectEmpty(currentTruck);
        } catch (error) {
            return res
                .status(400)
                .json({
                    message: 'Bad request',
                });
        }

        res.json({
            'truck': getRefactoredTruckObject(currentTruck),
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.put('/:id', checkDriverChangeAbility, async (req, res) => {
    try {
        const truckId = req.params.id;
        const {type} = req.body;
        const {error} = truckTypeValidator.validate({type});

        if (error) {
            return res
                .status(400)
                .json({
                    message: 'Wrong truck type',
                });
        }

        try {
            const updatedTruck = await truck.findByIdAndUpdate(truckId, {type});

            isObjectEmpty(updatedTruck);
        } catch (error) {
            return res
                .status(400)
                .json({
                    message: 'Bad request',
                });
        }

        res.json({
            message: 'Truck details changed successfully',
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.delete('/:id', checkDriverChangeAbility, async (req, res) => {
    try {
        const truckId = req.params.id;

        try {
            const deletedTruck = await truck.findByIdAndDelete(truckId);

            isObjectEmpty(deletedTruck);
        } catch (error) {
            return res
                .status(400)
                .json({
                    message: 'Bad request',
                });
        }

        res.json({
            message: 'Truck deleted successfully',
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

router.post('/:id/assign', checkDriverChangeAbility, async (req, res) => {
    try {
        const truckId = req.params.id;
        const {_id: userId} = req.currentUser;

        try {
            const updatedTruck = await truck.findByIdAndUpdate(
                truckId,
                {
                    assigned_to: userId,
                },
            );

            isObjectEmpty(updatedTruck);
        } catch (error) {
            return res
                .status(400)
                .json({
                    message: 'Bad request',
                });
        }

        await resetAssigned(userId, truckId);

        res.json({
            message: 'Truck assigned successfully',
        });
    } catch (error) {
        res
            .status(500)
            .json({
                message: 'Internal server error',
            });
    }
});

module.exports = router;
