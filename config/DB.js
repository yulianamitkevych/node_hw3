const mongoose = require('mongoose');

exports.connect = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URL);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};
