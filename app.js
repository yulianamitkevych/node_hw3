const http = require('http');
const app = require('./controllers/routes');

const DEFAULT_PORT = 8080;
const PORT = process.env.PORT || DEFAULT_PORT;

http.createServer(app).listen(PORT);
